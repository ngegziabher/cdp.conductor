package com.conductor.conductor;

import com.netflix.conductor.client.http.TaskClient;
import com.netflix.conductor.client.task.WorkflowTaskCoordinator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.netflix.conductor.client.worker.Worker;

@SpringBootApplication
public class ConductorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConductorApplication.class, args);

        TaskClient taskClient = new TaskClient();
        taskClient.setRootURI("http://localhost:8080/api/");

        int threadCount = 2;

        Worker worker1 = new ConWorker("preview_content");
        Worker worker2 = new ConWorker("publish_content");

        WorkflowTaskCoordinator.Builder builder = new WorkflowTaskCoordinator.Builder();
        WorkflowTaskCoordinator coordinator = builder.withWorkers(worker1, worker2)
                .withThreadCount(threadCount).withTaskClient(taskClient).build();

        coordinator.init();
	}

}
